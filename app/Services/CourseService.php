<?php

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Services;


use App\Models\Course;
use App\Jobs\CourseJob;
use App\Helpers\Constants;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class CourseService extends BaseService
{

    /**
     * @var Course
     */
    protected $repository;

    /**
     * Construct
     * @param Course
     */
    public function __construct(Course $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Call the course factory
     * @return void
     */
    public function courseFactory(CourseJob $job)
    {
        return $job->dispatch();
    }

    /**
     * Get All Available Courses
     * @return Paginator
     */
    public function getCourses(Request $request, UserService $userService)
    {
        $limit = $request->limit ?? Constants::DEFAULT_LIMIT;

        $paginated = $this->repository->paginate($limit);

        $collection =  $paginated->getCollection()
            ->map(function($course) use($userService) {
                return $userService->hasRegistered($course) ?
                    $course->setAttribute(
                        "enrolled", $userService->registeredCourseDate($course)) :
                        $course;
            })->all();

        return new LengthAwarePaginator(
            $collection,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), [
                'path' => Request::url(),
                'query' => [
                    'page' => $paginated->currentPage()
                ]
            ]
        );
    }

    /**
     * Export Courses
     * @return string
     */
    public function exportCourses(CourseExtractService $extractor)
    {
        return $extractor->generateFile(
            $this->repository->query()
        );
    }
}