<?php

namespace App\Exceptions;

use Exception;
use App\Helpers\Constants;
use App\Exceptions\JException;
use App\Helpers\ResponseHelper;
use App\Exceptions\ServiceException;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof JException) {
            return ResponseHelper::createErrorResponse($e->getMessage(), $e->getCode());
        } elseif ($e instanceof ValidationException) {
            $data = ["errors" => $e->validator->getMessageBag()->getMessages()];
            return ResponseHelper::createErrorResponse(
                Constants::FAILED_VALIDATION, 0, $data, 422
            );
        } elseif ($e instanceof ModelNotFoundException) {
            return ResponseHelper::createErrorResponse(
                Constants::RESOURCE_NOT_FOUND,
                5, [
                'errorMessage' => "No {$e->getModel()} records found for ids: "
                    . implode(",", (array)$e->getIds())
            ],
                404
            );
        } elseif ($e instanceof ServiceException) {
            return ResponseHelper::createErrorResponse($e->getMessage(), $e->getCode(), [
                'errors' => $e->getErrors()]);
        } elseif ($e instanceof MethodNotAllowedHttpException) {
            return ResponseHelper::createErrorResponse(Constants::ROUTE_NOT_FOUND, 3, [], 405);
        } elseif ($e instanceof UnauthorizedException) {
            return ResponseHelper::createErrorResponse(Constants::UNAUTHORIZED_USER, 12, [], 401);
        } else {
            $data = [
                'exception_message' => $e->getMessage(),
                'exception_trace' => $e->getTrace(),
            ];
            return ResponseHelper::createErrorResponse(
                Constants::EXCEPTION_THROWN, 4, $data
            );
        }
    }
}
