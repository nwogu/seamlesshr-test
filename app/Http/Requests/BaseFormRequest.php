<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public abstract function rules();

    public function getData()
    {
        return $this->only(array_keys($this->rules()));
    }

    public function getOnly(string  $key)
    {
        return ($this->only($key))[$key] ?? null;
    }
}