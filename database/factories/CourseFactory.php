<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'content' => $faker->text(),
        'instructor' => $faker->name,
        'rating' => $faker->numberBetween(1, 5),
        'duration' => $faker->numberBetween(1, 60)
    ];
});
