<?php
/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Helpers;

use Illuminate\Http\Response;

/**
 * Class ResponseHelper is a helper class that helps with creating responses
 * for the JSON API. It's benefit is providing a single-point-of-change and uniformity
 * across all entities that return a response(such as controllers and middlewares).
 */
class ResponseHelper
{

    const STATUS_SUCCESS = "success";
    const STATUS_ERROR = "error";

    public static function createSuccessResponse(array $data, string $message = ""): Response
    {
        return self::createResponse(self::STATUS_SUCCESS, $data, $message);
    }

    public static function createResponse(
        string $status = self::STATUS_SUCCESS,
        array $data = [],
        string $message = "",
        int $code = 0,
        $httpResponseCode = 200
    ): Response {
        $responseData = [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];
        if (!empty($code)) {
            $responseData['code'] = $code;
        }

        $header = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE, PATCH',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Origin, Authorization'
        ];
        return Response::create($responseData, $httpResponseCode, $header);
    }

    public static function createErrorResponse(
        $message,
        $errorCode = 0,
        array $data = [],
        $httpResponseCode = 401
    ): Response {
        return self::createResponse(self::STATUS_ERROR, $data, $message, $errorCode, $httpResponseCode);
    }
}