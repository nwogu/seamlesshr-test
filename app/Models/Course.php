<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'instructor', 'duration', 'rating'
    ];

    /**
     * Define Many Relationship with User
     */
    public function enrolled()
    {
        return $this->belongsToMany(User::class, "registered_courses")
            ->withTimestamps();
    }
}
