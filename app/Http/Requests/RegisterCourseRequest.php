<?php

namespace App\Http\Requests;

use App\Models\Course;
use App\Helpers\Constants;
use Illuminate\Support\Facades\Validator;

class RegisterCourseRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('is_numeric_array_and_exists', function($attribute, $value, $parameters) {
            foreach($value as $id) {
                if(! is_numeric($id)) return false;
                if (! Course::find($id)) return false;
            }
            return true;
        }, Constants::IS_NUMERIC_ARRAY_AND_EXISTS_ERROR);

       return [
           "courses" => "required|array|is_numeric_array_and_exists"
       ];
    }
}