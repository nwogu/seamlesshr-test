<?php

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Services;

use App\Models\Course;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;

class CourseExtractService
{

    /**
     * The disk name
     *
     * @var string
     */
    protected $disk;

    /**
     * The relative path to store the generated files on disk.
     *
     * @var string
     */
    protected $extractPath = "courses/extract";

    /**
     *
     * @var LaravelExcelWriter
     */
    protected $writer;

    /**
     * The Sheet Fields Mapped to Model Attributes
     * 
     * @var array
     */
    protected $fields = [
        "Instructor Name" => "instructor",
        "Duration" => "duration",
        "Rating" => "rating",
        "Content" => "content"
    ];

    public function __construct()
    {
        $this->disk = "public";
    }

    /**
     * Generate file name
     * the name of the file.
     * @return string the generated file name
     */
    protected function generateFileName()
    {
        return "courses_" . date("Y-m-d_H.i.s") . ".xls";
    }

    protected function writeDataToExcelFile(array $data)
    {
        $this->writer->getSheet()->appendRow($data);
    }

    /**
     * @param $fileName
     * @return mixed
     * @throws \Maatwebsite\Excel\Exceptions\LaravelExcelException
     */
    protected function saveReport($fileName)
    {
        Storage::disk($this->disk)->put($this->extractPath . $fileName, $this->writer->string());
        return Storage::disk($this->disk)->url($this->extractPath . $fileName);
    }


    /**
     * Determines the batch size to use in generating the courses sheet.
     *
     * @param Builder $builder the builder object to match courses to create
     * sheetfor.
     * @return int the batch size to be used
     */
    protected function getBatchSize(Builder $builder)
    {
        if ($builder->count() < 5000) {
            return 1000;
        }
        return 2000;
    }

    /**
     *
     * @param string $fileName
     * @param array $headers
     */
    protected function createExcelFile(string $fileName, array $headers = [])
    {
        $this->writer = Excel::create($fileName, function(LaravelExcelWriter $writer) use($headers) {
            $writer->sheet("Courses", function(LaravelExcelWorksheet $sheet) use($headers) {
                //set first row. i.e the headings row.
                if (!empty($headers))
                $sheet->row(1, $headers)
                    ->freezeFirstRow();
            });
        });
    }

     /**
     * Generates courses extracted file based on the query passed in via the
     * $builder argument.
     *
     * @param Builder $builder 
     * @return string the full link to download the export
     * file.
     * @throws \Exception
     */
    public function generateFile(Builder $builder)
    {
        $this->validateBuilder($builder);
        $batchSize = $this->getBatchSize($builder);
        $fileName = $this->generateFileName();
        $headers = $this->createFileHeaders();
        $this->createExcelFile($fileName, $headers);
        $builder->chunk($batchSize, function($courses) {
            foreach ($courses as $course) {
                $this->writeDataToExcelFile($this->generateRow($course));
            }
        });
        return $this->saveReport($fileName);
    }

    /**
     * Validate the builder
     */
    protected function validateBuilder(Builder $builder)
    {
        if (!$builder->getModel() instanceof Course) {
            throw new \Exception("Expected Query builder for "
            . "App\Models\Course model, got type of "
            . get_class($builder->getModel()) . " instead");
        }
    }

    /**
     * Create csv header row(i.e the first row).
     * 
     * @return array csv formatted string
     */
    protected function createFileHeaders()
    {
        return array_keys($this->fields);
    }

     /**
     * Create course extract row.
     * 
     * @param Course
     * @return array returns a csv formatted single row
     */
    protected function generateRow(Course $course)
    {
        $row = array_combine(array_keys($this->fields), array_fill(0, count($this->fields), ""));
        $courseArray = $course->toArray();
        foreach ($this->fields as $fieldName => $accessKey) {
            $row[$fieldName] = array_get($courseArray, $accessKey);
        }
        return $row;
    }
}