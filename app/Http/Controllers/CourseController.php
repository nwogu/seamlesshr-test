<?php

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Http\Controllers;

use App\Services\CourseService;
use App\Helpers\ResponseHelper;

class CourseController extends Controller
{

    /**
     * @var CourseService
     */
    protected $service;

    /**
     * Construct
     * @param CourseService
     */
    public function __construct(CourseService $service)
    {
        $this->service = $service;
    }

    /**
     * Create A New Course
     * @return Response
     */
    public function courseFactory()
    {
        $this->service->callCourseFactory();

        return ResponseHelper::createSuccessResponse([]);
    }

    /**
     * Get Course Information
     * @return Response
     */
    public function getCourses()
    {
        $courseInformation = $this->service->callGetCourses();

        return ResponseHelper::createSuccessResponse([
            "courses" => $courseInformation ]);
    }

     /**
     * Export Courses
     * @return Response
     */
    public function exportCourses()
    {
        $exportUrl = $this->service->callExportCourses();

        return ResponseHelper::createSuccessResponse([
            "export_url" => $exportUrl ]);
    }
}