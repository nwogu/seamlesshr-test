<?php

namespace App\Services;

use App\Models\User;
use App\Models\Course;
use App\Services\BaseService;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterCourseRequest;

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

class UserService extends BaseService
{

    /**
     * Repository
     * @var Model
     */
    protected $repository;

    /**
     * Construct
     * @param UserRepository
     */
    public function __construct(User $repository)
    {

        $this->repository = $repository;
    }

    /**
     * Create New User
     * @return array
     */
    public function createUser(UserRequest $request)
    {

        $data = $request->getData();

        $this->hasher($data);

        return $this->repository->create($data)->toArray();
    }

    /**
     * User Password Hasher
     * @param array
     * @return void
     */
    public function hasher(&$data, $field = "password")
    {
        if (isset($data[$field])) $data[$field] = Hash::make($data[$field]);
    }

    /**
     * Get User Payload
     * @return array
     * 
     */
    public function user()
    {
        $user = auth()->user();

        return $user;
    }

    /**
     * Register User For Courses
     * @return array
     */
    public function registerCourse(RegisterCourseRequest $request)
    {
        $this->user()->courses()->sync($request->getData()["courses"]);

        return $this->user()->courses->toArray();
    }

    /**
     * Checks For Registered Course
     * @param Course
     * @return bool
     */
    public function hasRegistered(Course $course)
    {
        return $this->user()
            ->courses()->where("course_id", $course->id)->first();
    }

    /**
     * Checks For Registered Course
     * @param Course
     * @return bool
     */
    public function registeredCourseDate(Course $course)
    {
        return $this->user()
            ->courses()->where("course_id", $course->id)
            ->first()->pivot->created_at;
    }
}