<?php

namespace App\Helpers;

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

 class Constants
 {
     const REQUIRED = "required";

     const DEFAULT_LIMIT = 20;

     const TOKEN_COULD_NOT_BE_REFRESHED = "Token could not be refreshed. Kindly Reauthenticate.";

     const TOKEN_IS_INVALID = "Token is Invalid";

     const TOKEN_IS_EXPIRED = "Token is Expired";

     const YOU_ARE_NOT_LOGGED = "You are not Logged In";

     const USER_NOT_FOUND = "User Not Found";

     const INVALID_LOGIN_DETAILS = "Email or Password is incorrect";

     const REFRESH_TIMEOUT = "+10minutes";

     const FAILED_VALIDATION = "Validation Failed";

     const RESOURCE_NOT_FOUND = "Resource not Found";

     const EXCEPTION_THROWN = "An Exception Occured";

     const ROUTE_NOT_FOUND = "Route does not exist";

     const UNAUTHORIZED_USER = "User does not have the right permissions";

     const IS_NUMERIC_ARRAY_AND_EXISTS_ERROR = "Course Id's Not Found";
 }