<?php

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Helpers\ResponseHelper;

class UserController extends Controller
{

    /**
     * @var UserService
     */
    protected $service;

    /**
     * Construct
     * @param UserService
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Create A New User
     * @return Response
     */
    public function createUser()
    {
        $userInformation = $this->service->callCreateUser();

        return ResponseHelper::createSuccessResponse([
            "user" => $userInformation ]);
    }

    /**
     * Get User Information
     * @return Response
     */
    public function getUser()
    {
        $userInformation = $this->service->user();

        return ResponseHelper::createSuccessResponse([
            "user" => $userInformation->toArray() ]);
    }

    /**
     * Register User for a Course
     * @return Response
     */
    public function registerCourse()
    {
        $courseRegisteredInformation = $this->service->callRegisterCourse();

        return ResponseHelper::createSuccessResponse([
            "course_registered_information" => $courseRegisteredInformation ]);
    }
}