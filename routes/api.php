<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//health check port
Route::any('/health-check', function() {
    //check if the database is connected
    \Illuminate\Support\Facades\DB::reconnect();
    //Check if user can be retrieved
    \App\Models\User::first();
    return \App\Helpers\ResponseHelper::createSuccessResponse([]);
});

Route::group(['middleware' => "api"], function() {

    //Auth/Register Routes
    Route::post('/register', 'UserController@createUser');
    Route::post('/login', 'AuthController@login');
    Route::get('/logout', 'AuthController@logout');
});

Route::group(['middleware' => ["api", "auth"]], function() {

    //Users
    Route::post('/register-course', 'UserController@registerCourse');

    //Courses
    Route::get('courses', 'CourseController@getCourses');
    Route::get('export-courses', 'CourseController@exportCourses');
    Route::get('/course-factory', 'CourseController@courseFactory');
});
