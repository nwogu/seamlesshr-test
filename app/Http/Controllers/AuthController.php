<?php

namespace App\Http\Controllers;

use App\Helpers\Constants;
use App\Services\AuthService;
use App\Helpers\ResponseHelper;

class AuthController extends Controller
{
    /**
     * @var AuthService
     */
    protected $service;

    /**
     * Construct
     * @param AuthService
     */
    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    /**
     * Login User
     * @return Response
     */
    public function login()
    {
        $userInformation = $this->service->login();

        if (empty($userInformation)) return ResponseHelper::createErrorResponse(
            Constants::INVALID_LOGIN_DETAILS);

        return ResponseHelper::createSuccessResponse($userInformation);
    }

    /**
     * Logout User
     * @return Response
     */
    public function logout()
    {
        $this->service->logout();

        return ResponseHelper::createSuccessResponse([]);
    }
}
