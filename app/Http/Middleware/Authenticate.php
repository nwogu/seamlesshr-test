<?php

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\ResponseHelper;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use App\Helpers\Constants;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token = JWTAuth::parseToken();
            $token->authenticate();
        } catch (JWTException $e) {
            if ($e instanceof TokenInvalidException) {
                return ResponseHelper::createErrorResponse(Constants::TOKEN_IS_INVALID, 401);
            } else if ($e instanceof TokenExpiredException) {
                return ResponseHelper::createErrorResponse(Constants::TOKEN_IS_EXPIRED, 401);
            } else {
                return ResponseHelper::createErrorResponse(Constants::YOU_ARE_NOT_LOGGED, 401);
            }
        }
        return $next($request);
    }
}