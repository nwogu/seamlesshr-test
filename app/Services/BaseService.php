<?php

namespace App\Services;
use Illuminate\Support\Str;

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

class BaseService
{
    /**
     * Handle Automatic Call of Dependency Injected Methods
     */
    public function __call($name, $arguments)
    {
        $method = Str::camel(substr($name, 4));

        $reflector = new \ReflectionMethod($this, $method);

        $args = $reflector->getParameters();

        $results = [];

        foreach ($args as $arg) {
            is_null($arg->getClass()) ?: $results[] = app()->make($arg->getClass()->getName());

        }
        
        $arguments = array_merge($results, $arguments);

        return $this->$method(...$arguments);
    }
}