<?php

namespace App\Services;

use App\Helpers\Constants;
use Illuminate\Http\Request;
use App\Exceptions\SException;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;

/**
 * @author Gabriel Nwogu <nwogugabriel@gmail.com>
 */

class AuthService extends BaseService
{
    /**
     * @var Request Request
     */
    protected $request;

    /**
     * Construct
     * @param Request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Login User
     * @return array
     */
    public function login(): array
    {
        if (! $token = auth()->attempt($this->request->only(["email", "password"]))) {
            return [];
        }

        return $this->respondWithToken($token);
    }

    /**
     * Construct Token Response Load
     * @return array
     */
    protected function respondWithToken($token): array
    {
        return [
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth()->factory()->getTTL() * 60,
            'user'          => auth()->user()->toArray()
        ];
    }

    /**
     * Log Out User
     * @return void
     */
    public function logout()
    {
        auth()->logout();
    }
}