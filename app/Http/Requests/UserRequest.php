<?php

namespace App\Http\Requests;

class UserRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        switch ($this->method()) {

            case "POST":

                if ($this->exists("name")){
                    $rules['name'] = "required|string";
                }
                if ($this->exists("email")) {
                    $rules['email'] = "required|string|unique:users";
                }
                if ($this->exists("password")) {
                    $rules['password'] = "required|string|confirmed|min:6";
                }

            break;

            case "PUT":

                if (!is_null($this->get("name"))) {
                    $rules['name'] = "string";
                }
                if (!is_null($this->get("email"))) {
                    $rules['email'] = "string";
                }
                if ($this->exists("password")) {
                    $rules['password'] = "string|confirmed|min:6";
                }

        }
        return $rules;
    }
}
